# 简介

页面被分为 title js css body 四部分

## 流程

  1. 点击链接
  2. XHR 请求页面
  3. 返回页面
  4. 将返回页面的 title 和 body 部分替换到当前页面，css 和 js 丢弃

这意味着使用 Turbolinks 的网站会让页面切换更快，但也要注意单页环境下产生的 js 陷阱。

# 事件的改变

# 三个 data-* 属性

# 与 Assets Pipeline 配合

Turbolinks 起效的条件之一是 head 内的静态文件不变，所以需要 Assets Pipeline 这样的静态文件打包工具配合


