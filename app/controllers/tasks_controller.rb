class TasksController < ApplicationController

  # GET /tasks
  def index
  end

  # GET /tasks/:id
  def show
  end

  # GET /tasks/new
  def new
  end

  # POST /tasks
  def create
  end

  # GET /tasks/:id/edit
  def edit
  end

  # PATCH /tasks/:id
  def update
  end

  # GET /tasks/:id/comments
  def comments
  end

  # POST /tasks/:id/add_comment
  def add_comment
  end

end
